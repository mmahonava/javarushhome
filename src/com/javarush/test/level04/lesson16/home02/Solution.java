package com.javarush.test.level04.lesson16.home02;

import java.io.*;

/* Среднее такое среднее
Ввести с клавиатуры три числа, вывести на экран среднее из них. Т.е. не самое большое и не самое маленькое.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String a1 = reader.readLine();
        int m = Integer.parseInt(a1);
        String b1 = reader.readLine();
        int n = Integer.parseInt(b1);
        String c1 = reader.readLine();
        int r = Integer.parseInt(c1);

        if ((m < n) && (m > r))
        {
            System.out.println(m);
        } else if ((n < m) && (n > r))
        {
            System.out.println(n);
        } else if ((r < m) && (r > n)) {
        System.out.println(r);
    }
    }
}
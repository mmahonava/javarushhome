package com.javarush.test.level04.lesson07.task04;

/* Положительные и отрицательные числа
Ввести с клавиатуры три целых числа. Вывести на экран количество положительных и количество отрицательных чисел в исходном наборе,
в следующем виде:
"количество отрицательных чисел: а", "количество положительных чисел: б", где а, б - искомые значения.
Пример для чисел 2 5 6:
количество отрицательных чисел: 0
количество положительных чисел: 3
Пример для чисел -2 -5 6:
количество отрицательных чисел: 2
количество положительных чисел: 1
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String a1 = reader.readLine();
        int a = Integer.parseInt(a1);
        String b1 = reader.readLine();
        int b = Integer.parseInt(b1);
        String c1 = reader.readLine();
        int c = Integer.parseInt(c1);

        int counta =0;
        int countb =0;
        if(a>0)
            counta++;
        else if(a<0)
            countb++;
        if(b>0)
            counta++;
        else if(b<0)
            countb++;
        if(c>0)
            counta++;
        else if(c<0)
            countb++;
        System.out.println("количество отрицательных чисел: " +countb);
        System.out.println("количество положительных чисел: " +counta);

        //напишите тут ваш код

    }
}

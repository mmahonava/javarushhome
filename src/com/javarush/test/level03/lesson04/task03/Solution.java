package com.javarush.test.level03.lesson04.task03;

/* StarCraft
Создать 10 зергов, 5 протосов и 12 терран.
Дать им всем уникальные имена.
*/

public class Solution
{
    public static void main(String[] args)
    {
        Zerg zerg1 = new Zerg();
        zerg1.name="asd";
        Zerg zerg2 = new Zerg();
        zerg2.name="jdfk";
        Zerg zerg3 = new Zerg();
        zerg3.name="asdfsdghd";
        Zerg zerg4 = new Zerg();
        zerg4.name="alkksd";
        Zerg zerg5 = new Zerg();
        zerg5.name="a;ugsd";
        Zerg zerg6 = new Zerg();
        zerg6.name="ayutuysd";
        Zerg zerg7 = new Zerg();
        zerg7.name="asyiutyiutyd";
        Zerg zerg8 = new Zerg();
        zerg8.name="axvcxvcsd";
        Zerg zerg9 = new Zerg();
        zerg9.name="aserwerd";
        Zerg zerg10 = new Zerg();
        zerg10.name="aserwerd";

        Protos protos1 = new Protos();
        protos1.name = "fggdfh";
        Protos protos2 = new Protos();
        protos2.name = "fggtedfh";
        Protos protos3 = new Protos();
        protos3.name = "fggtututdfh";
        Protos protos4 = new Protos();
        protos4.name = "yuufggdfh";
        Protos protos5 = new Protos();
        protos5.name = "fgpipiuoiyuytrgdfh";

        Terran terran1 = new Terran();
        terran1.name = "nbmb";
        Terran terran2 = new Terran();
        terran2.name = "nbiopoipuomb";
        Terran terran3 = new Terran();
        terran3.name = "trretrewenbmb";
        Terran terran4 = new Terran();
        terran4.name = "uiturtrnbmb";
        Terran terran5 = new Terran();
        terran5.name = "nbtrtyemb";
        Terran terran6 = new Terran();
        terran6.name = "nbiopiouoiumb";
        Terran terran7 = new Terran();
        terran7.name = "trytrertevxvcxnbmb";
        Terran terran8 = new Terran();
        terran8.name = "ttetwexnbmb";
        Terran terran9 = new Terran();
        terran9.name = "nsadadabmb";
        Terran terran10 = new Terran();
        terran10.name = "nlkjghfbmb";
        Terran terran11= new Terran();
        terran11.name = "nuryeerwerqbmb";
        Terran terran12 = new Terran();
        terran12.name = "nbjkhkjhkjhkjhkjmb";//напишите тут ваш код

    }

    public static class Zerg
    {
        public String name;
    }

    public static class Protos
    {
        public String name;
    }

    public static class Terran
    {
        public String name;
    }
}